import React from 'react';
import '../support/Support.css'

function Support() {

  return (    

    <div className="Support">            
        <div className='container'>
          <div className='row'>
            <div className='col-md-5 left-side'>
              <h2>We are happy to <span>help you</span></h2>
            </div>
            <div className='col-md-7 right-side'>
              <p>Wellcurve is on a mission to help every individual who has the zeal to work and passion to earn. If you have any suggestions or feedback</p>
              <div className='mail-box'>
                <div className='row mail-row'>
                  <div className='col-md-1 left'>                  
                    <a href='javascript:void(0)'><img src={require('../../../assets/images/home/mail.png')} alt="" /></a>
                  </div>
                  <div className='col-md-11 right'>
                    <h4>Reach us out at</h4>
                    <a href='mailto:vrinda@wellcurve.in'>vrinda@wellcurve.in</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>              
    </div>

  );
}

export default Support;
