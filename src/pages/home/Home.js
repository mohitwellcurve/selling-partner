
import React from 'react';
import '../home/Home.css';
import Customer from './customer-box/Customer';
import Banner from '../home/banner/Banner';
import Supplier from '../home/supplier-rewards/Supplier';
import Work from '../home/works/Work';
import Support from './support/Support';
import Registerform from './register/Registerform';

function Home() {

  return (    

    <div className="main">                
        <Banner></Banner>
        <Customer></Customer>
        <div className='reverse-section'>
        <Supplier></Supplier>
        <Registerform></Registerform>          
        </div>
        <Work></Work>
        <Support></Support>         
    </div>

  );
}

export default Home;
