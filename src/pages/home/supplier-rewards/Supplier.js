
import React from 'react';
import '../supplier-rewards/Supplier.css'




function Customer() {

  return (    

    <div className='Supplier_section' id='join'>
        <div className='container'>
            <h2>Here’s why should you join <span>Wellcurve</span></h2>
            <ul className='row'>
                <li className='col-md-3'>
                    <div className='text-box'>                    
                        <img src={require("../../../assets/images/home/icon-01.png")} alt="" />
                        <h3>Best Brands At Your DoorStep</h3>
                        <p>With a spread of over 100 top grocery brands, we bet you will find everything you need under one roof.</p>
                    </div>
                </li>
                <li className='col-md-3'>
                    <div className='text-box'>                    
                        <img src={require("../../../assets/images/home/icon-02.png")} alt="" />
                        <h3>Competitive Pricing</h3>
                        <p>We are on a mission to make healthier eating affordable and hence our prices are unbeatable.</p>
                    </div>
                </li>
                <li className='col-md-3'>
                    <div className='text-box'>                    
                        <img src={require("../../../assets/images/home/icon-03.png")} alt="" />
                        <h3>Irresistible Rewards and Deals</h3>
                        <p>With Wellcurve, earning monetary rewards is just a click away. We’re offering the best deals and incentives to help you earn ‘ghar bethe bethe’</p>
                    </div>
                </li>
                <li className='col-md-3'>
                    <div className='text-box'>                    
                        <img src={require("../../../assets/images/home/icon-04.png")} alt="" />
                        <h3>Be Your Own Boss</h3>
                        <p>We’re giving you a chance to become your own boss. You can work as per your convenience from the comfort of your own home.</p>
                    </div>
                </li>
            </ul>
        </div>
    </div>

  );
}

export default Customer;
