import React, { Component } from 'react';
import '../register/Registerform.css';
import axios from 'axios';



class Registerform extends Component{
   constructor(props) {
      super(props);
      this.state = {
        name: '', email: '', phone: '', statename: '', pincode: '', occupation: ''
      }
    }
   // handleAdd= async e =>{
   //    await this.setState({
   //       text : e.target.value
   //    })
   // };
   handleFormSubmit = e => {
      e.preventDefault();
      axios({
        method: 'post',
        url: "https://www.wellcurve.in/community/api/save_data_health_ki_misaal.php",
        headers: { 'content-type': 'application/json' },
        data: this.state
      })
        .then(result => {
          this.setState({
            mailSent: result.data.sent
          })
          this.state = {
            name: '', email: '', phone: '', statename: '', pincode: '', occupation: ''
          }
        })
        .catch(error => this.setState({ error: error.message }));
    };

   render()
   {
return(
   <div className="sign_up_form_main" id='register'>
   <div className='container'>
   <div>
  
</div>
  <h2>Register</h2>      
  <div className="sign_up_form">
     <div className="sign_up_form_inner row">
        <div className="col-md-6 left">
           <div className="main">            
               <video playsinline="" width="100%" height="100%" controls poster="https://stage.wellcurve.in/selling-partner/video/video-thumbnail.jpg" class="chrome">
                  <source src="https://stage.wellcurve.in/selling-partner/video/video.mp4" type="video/mp4" class="chrome"></source>
               </video>

              <div className="text">
                 <p>Heres how you become a Health ki Misaal <span>with Wellcurve</span></p>
              </div>
           </div>
        </div>
        <div className="col-md-6 right">
           <div className="inner">
              
                 <fieldset>
                 <ul class="row">
                  <li class="col-md-6">
                    <label>Name <sup>*</sup>
                    </label>
                    <div class="clear"></div>
                    <input type="text" class="gender" name="name" placeholder="Name" value={this.state.name} onChange={e => this.setState({ name: e.target.value })}/>
                  </li>
                  <li class="col-md-6 email">
                    <label>Email <sup>*</sup>
                    </label>
                    <div class="set">
                      <input type="email" placeholder="Email" value={this.state.email} onChange={e => this.setState({ email: e.target.value })}/>
                    </div>
                  </li>
                  <li class="col-md-6 phone">
                    <label>Phone <sup>*</sup>
                    </label>
                    <div class="set">
                      <input type="number" placeholder="Phone" value={this.state.phone} onChange={e => this.setState({ phone: e.target.value })}/>
                    </div>
                  </li>
                  <li class="col-md-6 awards">
                    <label>State <sup>*</sup>
                    </label>
                    <div class="set">
                      <input type="text" placeholder="State" value={this.state.statename} onChange={e => this.setState({ statename: e.target.value })}/>
                    </div>
                  </li>
                  <li class="col-md-6">
                  <label>Pincode <sup>*</sup>
                    </label>
                    <div class="set">
                      <input type="number" placeholder="Pincode" value={this.state.pincode} onChange={e => this.setState({ pincode: e.target.value })}/>
                    </div>
                  </li>
                  <li class="col-md-6">
                  <label>Select Occupation <sup>*</sup>
                    </label>
                    <div class="set">
                      <select value={this.state.occupation} onChange={e => this.setState({ occupation: e.target.value })}>
                        <option value="service">Service</option>
                        <option value="business">Business</option>
                        <option value="home maker">Home Maker</option>
                      </select>                      
                    </div>                    
                  </li>
                  <li class="col-md-12">
                    <label class="no_data"></label>
                    <input type="submit" onClick={e => this.handleFormSubmit(e)} class="submit" name="save" value="submit"/>
                  </li>
                </ul>   
                 </fieldset>
                <div className='thank-msg'>
                 {this.state.mailSent &&
                    <div>Thank you for contcting us.</div>
                  }
                </div>
           </div>
        </div>
     </div>
  </div>
</div>
</div>
      );
   }
}
export default Registerform;
