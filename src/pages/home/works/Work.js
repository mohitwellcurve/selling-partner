
import React from 'react';
import '../works/Work.css';

function Work() {

  return (    

    <div className='Work_section' id='work'>
        <div className='container'>
            <h2>How it works</h2>   
            <ul className='row'>
                <li className='col'>
                    <div className='box-text'>
                        <span>1</span>
                        <h3>Fill the form</h3>
                        <p>If you are on this page, then fill up the form with all the information that is asked for.</p>
                    </div>
                </li>
                <li className='col'>
                    <div className='box-text'>
                        <span>2</span>
                        <h3>Connect with Our Team</h3>
                        <p>Once your form is submitted, our team will reach out to you to share further details</p>
                    </div>
                </li>
                <li className='col'>
                    <div className='box-text'>
                        <span>3</span>
                        <h3>Join Whatsapp Group</h3>
                        <p>Join our official Whatsapp groups for information and daily updates</p>
                    </div>
                </li>
                <li className='col'>
                    <div className='box-text'>
                        <span>4</span>
                        <h3>Activate your Network</h3>
                        <p>To earn maximum rewards, activate your social circles to expedite sales</p>
                    </div>
                </li>
                <li className='col'>
                    <div className='box-text'>
                        <span>5</span>
                        <h3>Earn Rewards</h3>
                        <p>Your monetary incentives will be calculated on your monthly performance</p>
                    </div>
                </li>
            </ul>
        </div>
    </div>

  );
}

export default Work;
