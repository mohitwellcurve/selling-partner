
import React from 'react';
import '../banner/Banner.css';
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";

function Customer() {

  return (    

    <div className='banner_main'>

      <div className="banner_section desktop">            
          <Carousel autoPlay infiniteLoop={false} renderIndicator={false} showArrows={false} showThumbs={false} showStatus={false}>
              <div>                
                <a href='https://www.wellcurve.in/'><img src={require('../../../assets/images/home/banner-desktop.jpg')} alt="" /></a>
              </div>                
          </Carousel>            
      </div>

      <div className="banner_section mobile">            
          <Carousel autoPlay infiniteLoop={false} renderIndicator={false} showArrows={false} showThumbs={false} showStatus={false}>
              <div>                
              <a href='https://www.wellcurve.in/'><img src={require('../../../assets/images/home/banner-mobile.png')} alt="" /></a>
              </div>                
          </Carousel>            
      </div>

    </div>

  );
}

export default Customer;
