
import React from 'react';
import '../customer-box/Customer.css';




function Customer() {

  return (    

    <div className='customers_section'>
          <div className='container'>
          <div className='box'>
            <ul className='row'>
                <li className='col-md-4'>
                    <div className='text-box'>
                        <h3>100+ <span>Customers</span></h3>
                        <h6>Trust Wellcurve for their daily grocery and healthy food needs</h6>
                    </div>
                </li>                
                <li className='col-md-4'>
                    <div className='text-box'>
                        <h3>200+ <span>Health Experts</span></h3>
                        <h6>Are Part of Wellcurve’s Community to help users buy better</h6>
                    </div>
                </li>                
                <li className='col-md-4'>
                    <div className='text-box'>
                        <h3>100+ <span>Home Makers</span></h3>
                        <h6>Are working with Wellcurve and have proven to be the ultimate Health ki Misaal</h6>
                    </div>
                </li>                
            </ul>
            </div>
          </div>
        </div>

  );
}

export default Customer;
