
import React from 'react';
import '../header/Header.css';

function Header() {
  return (    
    <div className='header'>
        <div className="container">
            <div className="row">                
                <div className='col-md-5 left'>
                    <a href="https://www.wellcurve.in/" className='desktop'><img src={require('../../assets/images/global/header/logo.png')} alt="" /></a>
                    <a href="https://www.wellcurve.in/" className='mobile'><img src={require('../../assets/images/global/header/logo.png')} alt="" /></a>
                </div>                
                <div className='col-md-7 right'>
                  <ul>                    
                    <li><a href='#join'>Why join us</a></li>                    
                    <li><a href='#work'>How it works</a></li>
                    <li><a href="#register">Register Now</a></li>
                  </ul>                  
                </div>
            </div>        
        </div>              
    </div>
  );
}

export default Header;
