import React from 'react';
import '../footer/Footer.css';

function Footer () {
  return (    
    <div className='footer'>
        <div className="container">        
            <div className="row">
                <div className="col-md-6 profile_section">
                    <div className="social">                                                                                                                        
                        <ul>
                            <li>
                                <a href="https://www.facebook.com/wellcurve/" role="button">
                                <img src={require('../../assets/images/global/footer/social-icon-1.png')} alt="" />
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/accounts/login/?next=/wellcurvein/" role="button">
                                <img src={require('../../assets/images/global/footer/social-icon-2.png')} alt="" />
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/wellcurve?s=11&t=x8qeLjQjqQEY_m-H4U7WQw" role="button">
                                <img src={require('../../assets/images/global/footer/social-icon-3.png')} alt="" />
                                </a>
                            </li>                                        
                        </ul>                                        
                    </div>
                </div>
                <div className="col-md-6">
                    <p>2022 Wellcurve. All Rights Reserved</p>
                </div>                    
            </div>
        </div>           
    </div>
  );
}

export default Footer;
