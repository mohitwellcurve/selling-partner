import './App.css';
import React from 'react';
import Header from './global/header/Header';
import Footer from './global/footer/Footer';
import Home from './pages/home/Home';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './assets/style/global.css';
import './assets/style/media.css';


function App() {
  return (
    <div className="App">    
      <Header />
      <Home />           
      <Footer />        
    </div>
  );
  
}


export default App;
